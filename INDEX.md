# ListPCI

ListPCI is a PCI device listing utility which scans the PCI bus and displays data on all PCI devices it finds. It provides more verbose data than other comparable applications and is more flexible in its command line options. Using these, you may filter the devices reported to only those of a specified vendor ID or of a specified class. ListPCI can also return the number of matching devices in the DOS system variable ERRORLEVEL, making it easy to integrate into batch scripts.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## LISTPCI.LSM

<table>
<tr><td>title</td><td>ListPCI</td></tr>
<tr><td>version</td><td>1.02</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-07-11</td></tr>
<tr><td>description</td><td>Displays data on PCI devices</td></tr>
<tr><td>summary</td><td>ListPCI is a PCI device listing utility which scans the PCI bus and displays data on all PCI devices it finds. It provides more verbose data than other comparable applications and is more flexible in its command line options. Using these, you may filter the devices reported to only those of a specified vendor ID or of a specified class. ListPCI can also return the number of matching devices in the DOS system variable ERRORLEVEL, making it easy to integrate into batch scripts.</td></tr>
<tr><td>keywords</td><td>device, list, PCI</td></tr>
<tr><td>author</td><td>Mercury Thirteen (mercury0x0d@protonmail.com)</td></tr>
<tr><td>maintained&nbsp;by</td><td>Mercury Thirteen (mercury0x0d@protonmail.com)</td></tr>
<tr><td>primary&nbsp;site</td><td>https://www.mercurycoding.com/downloads.html#DOS</td></tr>
<tr><td>original&nbsp;site</td><td>https://www.mercurycoding.com/downloads.html#DOS</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GPLv2, with source code, no warranty](LICENSE)</td></tr>
</table>
